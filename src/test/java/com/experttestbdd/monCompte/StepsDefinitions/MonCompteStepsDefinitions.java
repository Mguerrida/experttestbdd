package com.experttestbdd.monCompte.StepsDefinitions;

import static org.junit.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import com.experttestbdd.monCompte.pages.MonComptePage;
import com.experttestbdd.utils.CommonMethods;
import com.experttestbdd.utils.Setup;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class MonCompteStepsDefinitions extends CommonMethods {
	
	   private CommonMethods commonMethods = new CommonMethods();
	    WebDriver driver;
		private MonComptePage monComptePage = new MonComptePage();
		public  MonCompteStepsDefinitions() {
			
			driver = Setup.driver;
			PageFactory.initElements(driver, MonComptePage.class);
	        }
	


@When("^i click on Mon compte menu$")
public void i_click_on_Mon_compte_menu() throws Throwable {
	monComptePage.goToMonCompte();
}

@Then("^I enter username \"([^\"]*)\"$")
public void i_enter_username(String arg1) throws Throwable {
monComptePage.ajoutUsername(arg1);
}

@Then("^I enter a password \"([^\"]*)\"$")
public void i_enter_a_password(String arg1) throws Throwable {
monComptePage.ajoutPassword(arg1);
}

@Then("^I click on identification button$")
public void i_click_on_identification_button() throws Throwable {
monComptePage.Login();
}

@Then("^I check the dashbord \"([^\"]*)\"$")
public void i_check_the_dashbord(String arg1) throws Throwable {

}

@When("^I click on Mon compte menu for wrong credentiials$")
public void i_click_on_Mon_compte_menu_for_wrong_credentiials() throws Throwable {
	monComptePage.goToMonCompte();
}

@When("^I enter username as \"([^\"]*)\" and password as \"([^\"]*)\"$")
public void i_enter_username_as_and_password_as(String arg1, String arg2) throws Throwable {
   monComptePage.ajoutUsername(arg1);
	monComptePage.ajoutPassword(arg2);
}
@When("^I click on identification button for wrong credentials$")
public void i_click_on_identification_button_for_wrong_credentials() throws Throwable {

}

@Then("^I check message as Identifiant inconnu\\. Vérifiez l’orthographe ou essayez avec votre adresse e-mail\\.$")
public void i_check_message_as_Identifiant_inconnu_Vérifiez_l_orthographe_ou_essayez_avec_votre_adresse_e_mail() throws Throwable {

}

@When("^I enter username as mar\\.maaroufi@gmail\\.com and password as soft$")
public void i_enter_username_as_mar_maaroufi_gmail_com_and_password_as_soft() throws Throwable {

}

@Then("^I check message as Erreur : le mot de passe que vous avez saisi pour l’adresse e-mail mar\\.maaroufi@gmail\\.com est incorrect\\.$")
public void i_check_message_as_Erreur_le_mot_de_passe_que_vous_avez_saisi_pour_l_adresse_e_mail_mar_maaroufi_gmail_com_est_incorrect() throws Throwable {

}

@When("^I enter username as  and password as $")
public void i_enter_username_as_and_password_as() throws Throwable {
  
}

@Then("^I check message as Erreur : Identifiant obligatoire\\.$")
public void i_check_message_as_Erreur_Identifiant_obligatoire() throws Throwable {

}

		
}


