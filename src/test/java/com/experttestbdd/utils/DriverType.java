package com.experttestbdd.utils;

public enum DriverType {
    CHROME,
    FIREFOX,
    IE,
    EDGE;
}